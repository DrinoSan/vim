" LUA Setup
lua require('plugins')
lua require('setup')
lua require('opts')

" Basic Setup
set path+=**
set cc=120
set wildignore+=**/node_modules/*
set wildignore+=**/.git/*
set wildignore+=*.log*
set wildignore+=*.map*
set noerrorbells
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set relativenumber nu
set cursorline
set nowrap
set smartindent
set autoindent
set noswapfile
set hidden
set nohlsearch
set nobackup
set undodir=~/.config/nvim/undo
set undofile
set incsearch
set scrolloff=8
set mouse=a
set list
set iskeyword=@,48-57,_,192-255,$
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
filetype plugin indent on

" Theming
set termguicolors
" let g:everforest_background = 'medium'
" let g:everforest_diagnostic_virtual_text = 'color'
set background=dark " or light if you want light mode
colorscheme gruvbox


" Define leader key
let mapleader = " "

" Basic mapping
nnoremap <C-t> :UndotreeShow<CR>
nnoremap <leader>y "+y
vnoremap <leader>y "+y
nnoremap <leader>Y gg"+yG
xnoremap <leader>p "_dP


" Moving Text
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Search and delete
nnoremap <expr> <leader>ds ":%s/" . input("Delete: ") . "//g\<CR>"

" Search and rename
nnoremap <expr> <leader>rn ":%s/" . input("Rename: ") . "/" . input("New name: ") . "/g\<CR>"

" Reload the config
nnoremap <silent> <leader>rl :w<CR>:so%<CR>

" Go Compiling
nnoremap <silent> <leader>gc :!go build %<CR>
nnoremap <silent> <leader>gr :!go run %<CR>

" Run current binary
nnoremap <silent> <leader>rr :./%<

" Run current python file
nnoremap <silent> <leader>pr :!python %<CR>

" Mapping for Nvim Tree
nnoremap <C-e> :NvimTreeToggle<CR>
nnoremap <leader>tr :NvimTreeRefresh<CR>
nnoremap <leader>tf :NvimTreeFindFile<CR>



" <c-k> will either expand the current snippet at the word or try to jump to
" the next position for the snippet.
inoremap <c-k> <cmd>lua return require'snippets'.expand_or_advance(1)<CR>

" Mapping for Quickfix List
nnoremap <silent> <leader>q :copen<CR>
nnoremap <silent> <leader>n :cn<CR>
nnoremap <silent> <leader>p :cp<CR>

" Mapping for panes movement
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-b> <C-w>H
noremap <C-x> <C-w>L
noremap <C-left> <C-w><
noremap <C-right> <C-w>>

" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" LSP matching configuration
" set completeopt=menu,menuone,noinsert,noselect

" Mapping for Telescope
nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fr <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').git_files()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>


" Mapping for floating term
"nnoremap <silent> <leader>t :FloatermToggle<CR>

" NerdCommenter spaces
let NERDSpaceDelims=1

