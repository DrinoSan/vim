return require('packer').startup(function()

    use ('ThePrimeagen/git-worktree.nvim')
	-- Lua
	use {
	  "folke/trouble.nvim",
	  requires = "kyazdani42/nvim-web-devicons",
	  config = function()
		require("trouble").setup {
		  -- your configuration comes here
		  -- or leave it empty to use the default settings
		  -- refer to the configuration section below
		}
	  end
	}

	use 'wbthomason/packer.nvim'
    -- other plugins...

    -- Completion framework:
    use 'hrsh7th/nvim-cmp' 

    -- LSP completion source:
    use 'hrsh7th/cmp-nvim-lsp'

    -- Useful completion sources:
    use 'hrsh7th/cmp-nvim-lua'
    use 'hrsh7th/cmp-nvim-lsp-signature-help'
    use 'hrsh7th/cmp-vsnip'                             
    use 'hrsh7th/cmp-path'                              
    use 'hrsh7th/cmp-buffer'                            
    use 'hrsh7th/vim-vsnip'                     

	use "ellisonleao/gruvbox.nvim"
	use 'nvim-lualine/lualine.nvim'
	use 'kyazdani42/nvim-web-devicons'
	use 'williamboman/mason-lspconfig.nvim'
	use 'williamboman/mason.nvim'
	use 'preservim/nerdcommenter'
	use {
	"windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
    }
	use 'neovim/nvim-lspconfig'
	use {
	  'nvim-tree/nvim-tree.lua',
	  requires = {
		'nvim-tree/nvim-web-devicons', -- optional, for file icons
	  },
	  tag = 'nightly' -- optional, updated every week. (see issue #1193)
	}
	use 'nvim-treesitter/nvim-treesitter'
	use 'nvim-treesitter/nvim-treesitter-context'
	use "nvim-lua/plenary.nvim"
	use 'simrat39/rust-tools.nvim'
	use 'nvim-telescope/telescope.nvim'
	use "lukas-reineke/indent-blankline.nvim"
	use 'lewis6991/gitsigns.nvim'

    
	use 'puremourning/vimspector'
	use 'tpope/vim-surround'
    -- other plugins...
end)
