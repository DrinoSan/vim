-- examples for your init.lua

-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded = 0
vim.g.loaded_netrwPlugin = 0

-- Setup Nvim Tree
require('nvim-tree').setup{
	auto_reload_on_write = false,
}
